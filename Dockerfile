# install OS layer to your dockerfile
FROM ubuntu:latest

# Label your dockerfile
LABEL authors="Luis Castillo  luis.scholars@gmail.com"

# install node v8 to run environment
RUN apt-get update 
RUN apt-get -y install curl gnupg 
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - 
RUN apt-get -y install nodejs 
RUN npm install -g yarn

# set app folder env variables - as per standard it should follow /liveperson/code/name_of_project
ENV LP_HOME="/liveperson"
ENV APP_CODE="${LP_HOME}/code/agent_widget"

# create folder where application will be running
RUN mkdir -p ${APP_CODE}/

# install external dependencies
COPY package.json ${APP_CODE}/

# install your global dependencies
RUN cd ${APP_CODE}/ && \
    yarn  install

# copy files that are required for the app to work - modify the lines accordingly
COPY . .

# change working dir
WORKDIR ${APP_CODE}

# start server and provide port that you will expose
EXPOSE 3000
CMD yarn start ${APP_CODE}
