const express = require('express');
const router = express.Router();
const path = require("path");
const request = require('request');
const rp = require('request-promise');
const passport = require('passport');
const dotenv = require('dotenv');
dotenv.config();
const {
    OAUTH2_CLIENT_ID,
    OAUTH2_CLIENT_SECRET,
    OAUTH2_TOKEN_URL,
    domain,
    accountId,
    lambdaUUID,
    userId
} = process.env;

router.get('/', (req, res, next) => {
    res.sendFile(path.resolve('index.html'));
})

router.post('/invokeFaaS', (req, res, next) => {
    console.log('entered invokefaas post route');
    console.log(req.body);
    const optionsToken = {
    	method: 'POST',
        url: `${OAUTH2_TOKEN_URL}`,
        headers: {
	  		'content-type': 'application/x-www-form-urlencoded'
        },
	  	form: {
	  		'client_id': `${OAUTH2_CLIENT_ID}`,
    		'client_secret': `${OAUTH2_CLIENT_SECRET}`,
	    	'grant_type': 'client_credentials'
	  	}
    };
    request(optionsToken, (error, response, body) => {
    	if(error){
    		console.log(error);
    		res.send(error);
    	} else {
    		let result = JSON.parse(response.body)
    		console.log('response: ', result);
            console.log('req body: ', req.body)
            console.log(domain, accountId, lambdaUUID)
            request({
                method: 'POST',
                url: `https://${domain}/api/account/${accountId}/lambdas/${lambdaUUID}/invoke?v=1`,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${result.access_token}`
                },
                json: true,
                body: {    
                    "timestamp": 0,
                    "headers": [],
                    "payload": {
                        transcript: req.body.transcript
                    }
                }
            }, (err, resp, body) => {
                if(err) console.log(err);
                else {
                    console.log(resp.body, body);
                    res.send(body);
                }
            })
        }
    });
})

//test route to receive transcript
router.post('/receiveTranscript', (req, res, next) => {
    console.log(req.body);
    if(req.body) res.sendStatus(200).end();
})

module.exports = router;