const express = require('express');
const port = process.env.PORT || 8000;
const path = require("path");
const routes = require('./routes');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join('public')));

app.use('/', routes);

app.listen(port, () => {
    console.log(`Listening to port ${port}. It's go time!`)
})

module.exports = app;