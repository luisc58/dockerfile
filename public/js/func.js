
const parseTranscript = (value) => {
    return value.reduce((transcript, line, i) => {
            if(line.textType == "html") {
                const text = line.text;
                let p1 = 0;
                let p2 = text.length-1;
                let closing;
                let opening;
                while(!closing || !opening) {
                    if(text[p1] == ">") {
                        closing = p1+1;
                    }
                    if(text[p2] == "<") {
                        opening = p2;
                    }
                    if(!closing) p1++;
                    if(!opening) p2--;
                }
                const newLine = text.slice(closing, opening).replace(/<br\s*\/?>/gi, '; ');
                return [...transcript, newLine];
            }
            else {
                return [...transcript, line.text];
            }
        }, [])
}

const sendPostRequest = (data) => {
    console.log('sendpostrequest accessed', data)
    try {
        const ajax = new XMLHttpRequest();
        // ajax.open('POST', 'http://localhost:8000/invokeFaaS');
        ajax.open('POST', 'https://lp-agent-widget.herokuapp.com/invokeFaaS');
        ajax.send(data);
    }
    catch(err) {
        console.log(err)
    }

    // request({
    //     url: 'https://lp-agent-widget.herokuapp.com/invokeFaaS',
    //     method: 'POST',
    //     data: {
    //         transcript: data
    //     }
    //     }, function(err, res, body) {
    //         if(err) console.log(err);
    //         else {
    //             console.log(res.body)
    //         }
    //     });
}

const setInnerHTML = (id, data) => {
    var y = document.getElementById(id).childNodes;
    var dataExists;
    if(id == "visitorInfo") dataExists = y.length > 2;
    else dataExists = y.length > 1;
    if(!dataExists) {
        const summary = document.getElementById(id);
        const p = document.createElement('p');
        p.textContent = data;
        summary.insertBefore(p, summary.firstElementChild);
    }
}